* Fix doubly named variable `NEWMAN_FAIL_ON_ERROR` (remove `NEWMAN_EXIT_ON_ERROR`)
* `NEWMAN_FAIL_ON_ERROR` default value is now `true`
* Allow specifying number of iterations through variable `NEWMAN_ITERATIONS_NUMBER` with 2 as default value
* Add the `cli` report in log output in addition to `junit`
* Add `junit` report as user-reachable artifact in addition to the report integration
